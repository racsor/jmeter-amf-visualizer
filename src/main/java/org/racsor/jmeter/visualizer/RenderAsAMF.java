/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.racsor.jmeter.visualizer;

import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.visualizers.ResultRenderer;
import org.apache.jmeter.visualizers.SamplerResultTab;
import org.apache.jmeter.visualizers.ViewResultsFullVisualizer;
import org.racsor.jmeter.flex.messaging.util.AMFPanel;

public class RenderAsAMF extends SamplerResultTab implements ResultRenderer {

   private static final String X_AMF = "application/x-amf"; // $NON-NLS-1$

    /** {@inheritDoc} */
    public void renderResult(SampleResult sampleResult) {
        // get the text response and image icon
        // to determine which is NOT null
        String response = ViewResultsFullVisualizer.getResponseAsString(sampleResult);
        showRenderedAMFResponse(response, sampleResult);
    }

    private void showRenderedAMFResponse(String response, SampleResult sampleResult) {
        if (response == null) {
            results.setText("");
            return;
        }
        AMFPanel amfTreePanel = new AMFPanel();
        amfTreePanel.setBytes("", sampleResult.getResponseData());
    	results.setCaretPosition(0);
        resultsScrollPane.setViewportView(amfTreePanel);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "AMF"; // $NON-NLS-1$
    }

}
