package org.racsor.jmeter.flex.messaging.io.amf;

import java.io.IOException;
import java.io.InputStream;

import org.racsor.jmeter.flex.messaging.io.MessageIOConstants;

public class AmfMessageDeserializer {

    /**
     * @ignore
     */
    private Amf0Input amfIn;
    /*
     * DEBUG LOGGING.
     */
    protected AmfTrace debugTrace;
    protected boolean isDebug;

    public AmfMessageDeserializer() {
    }

    /**
     * Establishes the context for reading in data from the given InputStream. A
     * null value can be passed for the trace parameter if a record of the AMF
     * data should not be made.
     */
    public void initialize(InputStream in, AmfTrace trace) {
	amfIn = new Amf0Input();
	amfIn.setInputStream(in);

	debugTrace = trace;
	isDebug = debugTrace != null;
	amfIn.setDebugTrace(debugTrace);
    }

    public void readMessage(ActionMessage m) throws ClassNotFoundException, IOException {
	if (m == null)
	    m = new ActionMessage();
	if (isDebug)
	    debugTrace.startRequest("Deserializing AMF/HTTP request");

	// Read packet header
	int version = amfIn.readUnsignedShort();

	if (version != MessageIOConstants.AMF0 && version != MessageIOConstants.AMF3) {
	    //Unsupported AMF version {version}.
	    RuntimeException ex = new RuntimeException("Unsupported AMF version " + version);
	    throw ex;
	}

	m.setVersion(version);

	if (isDebug)
	    debugTrace.version(version);

	// Read headers
	int headerCount = amfIn.readUnsignedShort();
	for (int i = 0; i < headerCount; ++i) {
	    MessageHeader header = new MessageHeader();
	    m.addHeader(header);
	    readHeader(header, i);
	}

	// Read bodies
	int bodyCount = amfIn.readUnsignedShort();
	for (int i = 0; i < bodyCount; ++i) {
	    MessageBody body = new MessageBody();
	    m.addBody(body);
	    readBody(body, i);
	}
    }

    /**
     * Deserializes a message from the input stream. This message differs
     * slightly from the BlazeDS implementation in that a message parameter is
     * not required. If no message is passed in the function returns the newly
     * created message.
     */
    public void readHeader(MessageHeader header, int index) throws ClassNotFoundException, IOException {
	String name = amfIn.readUTF();
	header.setName(name);
	boolean mustUnderstand = amfIn.readBoolean();
	header.setMustUnderstand(mustUnderstand);

	amfIn.readInt(); // Length

	amfIn.reset();
	Object data;

	if (isDebug)
	    debugTrace.startHeader(name, mustUnderstand, index);

	try {
	    data = readObject();
	} catch (Exception ex) {
	    RuntimeException ex1 = new RuntimeException("Client.Header.Encoding");
	    throw ex1;
	}

	header.setData(data);

	if (isDebug)
	    debugTrace.endHeader();
    }

    /**
     * Deserialize a message body from the input stream.
     * 
     * @param body
     *            - will hold the deserialized message body
     * @param index
     *            message index for debugging
     * @throws IOException
     *             thrown by the underlying stream
     * @throws ClassNotFoundException
     *             if we don't find the class for the body data.
     */
    public void readBody(MessageBody body, int index) throws ClassNotFoundException, IOException {
	String targetURI = amfIn.readUTF();
	body.setTargetURI(targetURI);
	String responseURI = amfIn.readUTF();
	body.setResponseURI(responseURI);

	amfIn.readInt(); // Length

	amfIn.reset();
	Object data;

	if (isDebug)
	    debugTrace.startMessage(targetURI, responseURI, index);

	try {
	    data = readObject();
	} catch (Exception ex) {
	    RuntimeException ex1 = new RuntimeException(ex);
	    throw ex1;
	}

	body.setData(data);

	if (isDebug)
	    debugTrace.endMessage();
    }

    public Object readObject() throws ClassNotFoundException, IOException {
	return amfIn.readObject();
    }

}
