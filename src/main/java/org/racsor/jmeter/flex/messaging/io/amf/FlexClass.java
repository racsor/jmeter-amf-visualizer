package org.racsor.jmeter.flex.messaging.io.amf;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Map;

import org.racsor.jmeter.flex.messaging.util.UUIDUtils;



public class FlexClass implements Externalizable {
    private static final short HAS_NEXT_FLAG = 128;
    private static final short BODY_FLAG = 1;
    private static final short CLIENT_ID_FLAG = 2;
    private static final short DESTINATION_FLAG = 4;
    private static final short HEADERS_FLAG = 8;
    private static final short MESSAGE_ID_FLAG = 16;
    private static final short TIMESTAMP_FLAG = 32;
    private static final short TIME_TO_LIVE_FLAG = 64;
    private static final short CLIENT_ID_BYTES_FLAG = 1;
    private static final short MESSAGE_ID_BYTES_FLAG = 2;
    private static byte CORRELATION_ID_FLAG = 1;
    private static byte CORRELATION_ID_BYTES_FLAG = 2;


    protected Object clientId;
    protected String destination;
    protected String messageId;
    protected long timestamp;
    protected long timeToLive;
    
    protected Map headers;
    protected Object body;

    private byte[] clientIdBytes;
    private byte[] messageIdBytes;
    protected String correlationId;
    protected byte[] correlationIdBytes;

	public void readExternal(ObjectInput input) throws IOException,
			ClassNotFoundException {
 
		
        short[] flagsArray = readFlags(input);

        for (int i = 0; i < flagsArray.length; i++)
        {
            short flags = flagsArray[i];
            short reservedPosition = 0;

            if (i == 0)
            {
                if ((flags & BODY_FLAG) != 0)
                    body = input.readObject();
        
                if ((flags & CLIENT_ID_FLAG) != 0)
                    clientId = input.readObject();
        
                if ((flags & DESTINATION_FLAG) != 0)
                    destination = (String)input.readObject();
        
                if ((flags & HEADERS_FLAG) != 0)
                    headers = (Map)input.readObject();
        
                if ((flags & MESSAGE_ID_FLAG) != 0)
                    messageId = (String)input.readObject();
        
                if ((flags & TIMESTAMP_FLAG) != 0)
                    timestamp = ((Number)input.readObject()).longValue();
        
                if ((flags & TIME_TO_LIVE_FLAG) != 0)
                    timeToLive = ((Number)input.readObject()).longValue();

                reservedPosition = 7;
            }
            else if (i == 1)
            {
                if ((flags & CLIENT_ID_BYTES_FLAG) != 0)
                {
                    clientIdBytes = (byte[])input.readObject();
                    clientId = UUIDUtils.fromByteArray(clientIdBytes);
                }
        
                if ((flags & MESSAGE_ID_BYTES_FLAG) != 0)
                {
                    messageIdBytes = (byte[])input.readObject();
                    messageId = UUIDUtils.fromByteArray(messageIdBytes);
                }

                reservedPosition = 2;
            }

            // For forwards compatibility, read in any other flagged objects to
            // preserve the integrity of the input stream...
            if ((flags >> reservedPosition) != 0)
            {
                for (short j = reservedPosition; j < 6; j++)
                {
                    if (((flags >> j) & 1) != 0)
                    {
                        input.readObject();
                    }
                }
            }
        }
        
		
		flagsArray = readFlags(input);
        for (int i = 0; i < flagsArray.length; i++)
        {
            short flags = flagsArray[i];
            short reservedPosition = 0;

            if (i == 0)
            {
                if ((flags & CORRELATION_ID_FLAG) != 0)
                    correlationId = (String)input.readObject();

                if ((flags & CORRELATION_ID_BYTES_FLAG) != 0)
                {
                    correlationIdBytes = (byte[])input.readObject();
                    correlationId = UUIDUtils.fromByteArray(correlationIdBytes);
                }

                reservedPosition = 2;
            }

            // For forwards compatibility, read in any other flagged objects
            // to preserve the integrity of the input stream...
            if ((flags >> reservedPosition) != 0)
            {
                for (short j = reservedPosition; j < 6; j++)
                {
                    if (((flags >> j) & 1) != 0)
                    {
                        input.readObject();
                    }
                }
            }
        }

        flagsArray = readFlags(input);
        for (int i = 0; i < flagsArray.length; i++)
        {
            short flags = flagsArray[i];
            short reservedPosition = 0;

            // For forwards compatibility, read in any other flagged objects
            // to preserve the integrity of the input stream...
            if ((flags >> reservedPosition) != 0)
            {
                for (short j = reservedPosition; j < 6; j++)
                {
                    if (((flags >> j) & 1) != 0)
                    {
                        input.readObject();
                    }
                }
            }
        }

	}

    /**
     * @exclude
     * To support efficient serialization for SmallMessage implementations,
     * this utility method reads in the property flags from an ObjectInput
     * stream. Flags are read in one byte at a time. Flags make use of
     * sign-extension so that if the high-bit is set to 1 this indicates that
     * another set of flags follows.
     * 
     * @return The array of property flags. 
     */
    protected short[] readFlags(ObjectInput input) throws IOException
    {
        boolean hasNextFlag = true;
        short[] flagsArray = new short[2];
        int i = 0;

        while (hasNextFlag)
        {
            short flags = (short)input.readUnsignedByte();
            if (i == flagsArray.length)
            {
                short[] tempArray = new short[i*2];
                System.arraycopy(flagsArray, 0, tempArray, 0, flagsArray.length);
                flagsArray = tempArray;
            }

            flagsArray[i] = flags;

            if ((flags & HAS_NEXT_FLAG) != 0)
                hasNextFlag = true;
            else
                hasNextFlag = false;

            i++;
        }

        return flagsArray;
    }

	public void writeExternal(ObjectOutput output) throws IOException {
 

        short flags = 0;

        if (clientIdBytes == null && clientId instanceof String)
            clientIdBytes = UUIDUtils.toByteArray((String)clientId);

        if (messageIdBytes == null)
            messageIdBytes = UUIDUtils.toByteArray(messageId);

        if (body != null)
            flags |= BODY_FLAG;

        if (clientId != null && clientIdBytes == null)
            flags |= CLIENT_ID_FLAG;

        if (destination != null)
            flags |= DESTINATION_FLAG;

        if (headers != null)
            flags |= HEADERS_FLAG;

        if (messageId != null && messageIdBytes == null)
            flags |= MESSAGE_ID_FLAG;

        if (timestamp != 0)
            flags |= TIMESTAMP_FLAG;

        if (timeToLive != 0)
            flags |= TIME_TO_LIVE_FLAG;

        if (clientIdBytes != null || messageIdBytes != null)
            flags |= HAS_NEXT_FLAG;

        output.writeByte(flags);

        flags = 0;

        if (clientIdBytes != null)
            flags |= CLIENT_ID_BYTES_FLAG;

        if (messageIdBytes != null)
            flags |= MESSAGE_ID_BYTES_FLAG;

        if (flags != 0)
            output.writeByte(flags);

        if (body != null)
            output.writeObject(body);

        if (clientId != null && clientIdBytes == null)
            output.writeObject(clientId);

        if (destination != null)
            output.writeObject(destination);

        if (headers != null)
            output.writeObject(headers);

        if (messageId != null && messageIdBytes == null)
            output.writeObject(messageId);

        if (timestamp != 0)
            output.writeObject(new Long(timestamp));

        if (timeToLive != 0)
            output.writeObject(new Long(timeToLive));

        if (clientIdBytes != null)
            output.writeObject(clientIdBytes);

        if (messageIdBytes != null)
            output.writeObject(messageIdBytes);

		
		
		
        if (correlationIdBytes == null)
            correlationIdBytes = UUIDUtils.toByteArray(correlationId);

        flags = 0;

        if (correlationId != null && correlationIdBytes == null)
            flags |= CORRELATION_ID_FLAG;

        if (correlationIdBytes != null)
            flags |= CORRELATION_ID_BYTES_FLAG;

        output.writeByte(flags);

        if (correlationId != null && correlationIdBytes == null)
            output.writeObject(correlationId);

        if (correlationIdBytes != null)
            output.writeObject(correlationIdBytes);
		

        flags = 0;
        output.writeByte(flags);
	}

}
