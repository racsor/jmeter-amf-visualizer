package org.racsor.jmeter.flex.messaging.util;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;

import com.thoughtworks.xstream.XStream;

import flex.messaging.io.MessageDeserializer;
import flex.messaging.io.MessageIOConstants;
import flex.messaging.io.MessageSerializer;
import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.ActionContext;
import flex.messaging.io.amf.ActionMessage;
import flex.messaging.io.amf.AmfMessageDeserializer;
import flex.messaging.io.amf.AmfMessageSerializer;
import flex.messaging.io.amf.AmfTrace;
import flex.messaging.io.amf.MessageBody;

/**
 * Class to serialize and deserialize AMF request/response to xmlSerialization
 * 
 * @author oruizs
 *
 */
public class UtilsFlexMessage {
    ActionContext context;
    ActionMessage message;
    String targetURI = "";
    String responseURI = "";
    String content;

    public ActionMessage getMessage() {
	return message;
    }

    public void setMessage(ActionMessage message) {
	this.message = message;
    }

    SerializationContext dsContext;
    MessageDeserializer deserializer;
    AmfTrace trace;

    public UtilsFlexMessage() {
	trace = new AmfTrace();

	/**
	 * CREATE A DESERIALIZER FOR SAMPLE AMF REQUEST
	 */
	context = new ActionContext();
	message = new ActionMessage();
	content=new String();
	context.setRequestMessage(message);
	trace.startRequest("----------Deserializing AMF/HTTP request-----");
	dsContext = SerializationContext.getSerializationContext();
	dsContext.instantiateTypes = false;

	deserializer = new AmfMessageDeserializer();
    }

    public void parseInputStream(InputStream din) {
	try {
	    deserializer.initialize(dsContext, din, trace);
	    /**
	     * RECORD TIME TO DESERIALIZE THE SAMPLE MESSAGE AS OUR TEST
	     * METRIC...
	     */

	    long start = System.currentTimeMillis();

	    deserializer.readMessage(message, context);

	    long finish = System.currentTimeMillis();
	    trace.endMessage();
	    long duration = finish - start;

	    /**
	     * PRINT TRACE OUTPUT
	     */
	    System.out.print(trace.toString());
	    System.out.println("AMF Deserialization Time: " + duration + "ms");
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public String updateFlexMessage(String from, String to) {
	content = messageToXML();
	content= StringUtils.replace(content, from, to);
	return content;
    }

    public void serializeMessage(String toFile,String contenido) {
	try {
	    FileUtils.writeByteArrayToFile(new File(toFile), serializeMessage(contenido));
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }
    public byte[] serializeMessage(String contenido) {
	try {
	    ActionMessage requestMessage = new ActionMessage();
	    
	    ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
	    MessageBody amfMessage = new MessageBody(targetURI, responseURI, new com.thoughtworks.xstream.XStream().fromXML(contenido));
	    requestMessage.addBody(amfMessage);
	    MessageSerializer amfMessageSerializer = new AmfMessageSerializer();
	    amfMessageSerializer.initialize(dsContext, outBuffer, trace);
	    amfMessageSerializer.writeMessage(requestMessage);
	    return outBuffer.toByteArray();
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    return null;
	}
	
    }

    public String messageToXML() {
	String content = "";

	ArrayList<MessageBody> messages = message.getBodies();
	for (MessageBody messageBody : messages) {
	    targetURI = messageBody.getTargetURI();
	    responseURI = messageBody.getResponseURI();

	    content = new XStream().toXML(messageBody.getData());
	    if (targetURI.endsWith(MessageIOConstants.RESULT_METHOD)) {
		content = new com.thoughtworks.xstream.XStream().toXML(messageBody.getData());
	    } else if (targetURI.endsWith(MessageIOConstants.STATUS_METHOD)) {
		// String exMessage = "Server error";
		// HttpResponseInfo responseInfo = generateHttpResponseInfo();
		// ServerStatusException exception = new ServerStatusException(
		// exMessage, message.getData(), responseInfo );

		content = new com.thoughtworks.xstream.XStream().toXML(messageBody.getData());
		// throw exception;
	    }
	}
	return content;
    }

}
