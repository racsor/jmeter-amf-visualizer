package org.racsor.jmeter.flex.messaging.util;

import javax.servlet.ServletRequest;

import org.racsor.jmeter.flex.messaging.io.amf.AmfMessageDeserializer;


public class AMFUtils {
	private AmfMessageDeserializer amfDeserializer;
	public boolean isAmfRequest(ServletRequest request){
		return true;
//		if (!request.getContentType())
//			return false;
//		
//		var contentType = request.contentType.split(";")[0];
//		contentType = trim(contentType);
//		return contentTypes[contentType];
//		
	}
	
/*	
	parseRequestAMF: function(file) {
		
		// Debug
		if (AMFXTrace.DBG_AMFREQUEST)
			AMFXTrace.sysout("amfViewerModel.utils.parseRequestAMF");
		
		var postHeaders = this.parsePostHeaders(file);		
		
		// Debug
		if (AMFXTrace.DBG_AMFREQUEST)
			AMFXTrace.sysout("amfViewerModel.utils.parseRequestAMF.postHeaders",postHeaders);	
		
		var length = postHeaders['content-length'];
		var offset = parseInt(length) * -1;
		var request = file.request;
		var is = request.QueryInterface(Ci.nsIUploadChannel).uploadStream;
		var ss = is.QueryInterface(Ci.nsISeekableStream);
		ss.seek(NS_SEEK_END, offset);
				
		if (!this.amfDeserializer) {
			// Debug
			if (AMFXTrace.DBG_AMFREQUEST)
				AMFXTrace.sysout("amfViewerModel.utils.parseRequestAMF.initDeserializer");
			this.amfDeserializer = new AMFLib.AmfMessageDeserializer();
		}
		
		this.amfDeserializer.initialize(ss);
		var obj = this.amfDeserializer.readMessage();
		
		return obj;
	},
	
	parseResponseAMF: function(file) {
		
		// Debug
		if (AMFXTrace.DBG_AMFRESPONSE)
			AMFXTrace.sysout("amfViewerModel.utils.parseResponseAMF");
		
		if (!file.responseStream) {
			var e = new Error($STR("amfexplorer.missingResponse","strings_amfExplorer"));
			throw e;
		}			
		
		var is = file.responseStream;
		var ss = is.QueryInterface(Ci.nsISeekableStream);
		ss.seek(NS_SEEK_SET, 0);
		
		if (!this.mfDeserializer) {
			// Debug
			if (AMFXTrace.DBG_AMFRESPONSE)
				AMFXTrace.sysout("amfViewerModel.utils.parseResponseAMF.initDeserializer");
			this.amfDeserializer = new AMFLib.AmfMessageDeserializer();
		}
		
		this.amfDeserializer.initialize(ss);
		var obj = this.amfDeserializer.readMessage();
		
		return obj;
	},
	
	parsePostHeaders: function(file) {
		
		// Debug
		if (AMFXTrace.DBG_AMFREQUEST)
			AMFXTrace.sysout("amfViewerModel.utils.parsePostHeaders");
		
		var text = file.postText;
		if (text == undefined)
			return null;	 
		
		var postHeaders = {};
		
		var divider = "\r\n\r\n";
		var headerEnd = text.indexOf(divider);
		var headers = text.substr(0,headerEnd);		
		
		var parts = headers.split("\r\n");
		for (var i=0; i<parts.length; i++)
		{
			var part = parts[i].split(":");
			if (part.length != 2)
				continue;
			
			postHeaders[trim(part[0].toLowerCase())] = trim(part[1].toLowerCase());
		}
		
		return postHeaders;
	},
	
	getCacheKey: function(request) {
		var is = request.QueryInterface(Ci.nsIUploadChannel).uploadStream;
		var ss = is.QueryInterface(Ci.nsISeekableStream);
		ss.seek(NS_SEEK_SET,0);			
		var ch = Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);
		ch.init(ch.MD5);
		ch.updateFromStream(ss, ss.available());
		var hash = ch.finish(true);
		return hash;   
	},
	
	saveStream: function(stream, filename){
		try {
		
			var dirService = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties);
			
			var aFile = dirService.get("ProfD", Ci.nsIFile);
			aFile.append("amfexplorer");
			aFile.append(filename);
			aFile.createUnique(Ci.nsIFile.NORMAL_FILE_TYPE, 600);
			
			var fos = Cc["@mozilla.org/network/safe-file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
			fos.init(aFile, 0x04 | 0x08 | 0x20, 0600, 0); // write, create, truncate
			var bos = Cc["@mozilla.org/network/buffered-output-stream;1"].createInstance(Ci.nsIBufferedOutputStream);
			bos.init(fos, 8192);
			
			for (var count = stream.available(); count; count = stream.available()) 
				bos.writeFrom(stream, count);
			
			// Debug
			if (AMFXTrace.DBG_CACHELISTENER) 
				AMFXTrace.sysout("saveResponse: " + filename);
			
		} 
		catch (exc) {
			// Debug
			if (AMFXTrace.DBG_CACHELISTENER) 
				AMFXTrace.sysout("saveResponse ERROR", exc);			
		}
		finally {
			if (fos) {
				if (fos instanceof Ci.nsISafeOutputStream) {
					fos.finish();
				}
				else {
					fos.close();
				}
			}
		}
	},
	
	safeGetName: function(request) {
		try {
			return request.name;
		} catch (exc) {
			return null;
		}
	}

*/
	}
