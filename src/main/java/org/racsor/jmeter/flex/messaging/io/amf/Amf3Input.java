package org.racsor.jmeter.flex.messaging.io.amf;

import java.io.Externalizable;
import java.io.IOException;
import java.io.UTFDataFormatException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Amf3Input extends AbstractAmfInput implements Amf3Types {
	public Map<String, String> classAliasRegistry = new HashMap<String, String>();
	public Map<String, String> classesFlex = new HashMap<String, String>();
//	public String[] classesFlex = new String[]{"AcknowledgeMessageExt","AsyncMessageExt","CommandMessageExt","AcknowledgeMessage","ErrorMessage","CommandMessage"};

	protected List objectTable;
	protected boolean isDebug = true;

	/**
	 * @exclude
	 */
	protected List stringTable;

	/**
	 * @exclude
	 */
	protected List traitsTable;

	public Amf3Input() {
		stringTable = new ArrayList(64);
		objectTable = new ArrayList(64);
		traitsTable = new ArrayList(10);
		classAliasRegistry.put("DSK",
				"flex.messaging.messages.AcknowledgeMessageExt");
		classAliasRegistry
				.put("DSA", "flex.messaging.messages.AsyncMessageExt");
		classAliasRegistry.put("DSC",
				"flex.messaging.messages.CommandMessageExt");
		classesFlex.put("AcknowledgeMessageExt", "AcknowledgeMessageExt");
//		classesFlex.put("AcknowledgeMessage", "AcknowledgeMessage");
		classesFlex.put("CommandMessageExt", "CommandMessageExt");
		classesFlex.put("CommandMessage", "CommandMessage");
		classesFlex.put("AsyncMessageExt", "AsyncMessageExt");
//		classesFlex.put("ErrorMessage", "ErrorMessage");
	}

	public void reset() {
		stringTable.clear();
		objectTable.clear();
		traitsTable.clear();
	}

	public Object saveObjectTable() {
		Object table = objectTable;
		objectTable = new ArrayList(64);
		return table;
	}

	public void restoreObjectTable(Object table) {
		objectTable = (ArrayList) table;
	}

	public Object saveTraitsTable() {
		Object table = traitsTable;
		traitsTable = new ArrayList(10);
		return table;
	}

	public void restoreTraitsTable(Object table) {
		traitsTable = (ArrayList) table;
	}

	public Object saveStringTable() {
		Object table = stringTable;
		stringTable = new ArrayList(64);
		return table;
	}

	public void restoreStringTable(Object table) {
		stringTable = (ArrayList) table;
	}

	public Object readObject() throws ClassNotFoundException, IOException {
		int type = in.readByte();
		Object value = readObjectValue(type);
		return value;
	}

	protected Object readObjectValue(int type) throws ClassNotFoundException,
			IOException {
		Object value = null;

		switch (type) {
		case kStringType:
			value = readString();

			if (isDebug)
				trace.writeString((String) value);
			break;

		case kObjectType:
			value = readScriptObject();
			break;

		case kArrayType:
			value = readArray();
			break;

		case kFalseType:
			value = Boolean.FALSE;

			if (isDebug)
				trace.write(value);
			break;

		case kTrueType:
			value = Boolean.TRUE;

			if (isDebug)
				trace.write(value);
			break;

		case kIntegerType:
			int i = readUInt29();
			// Symmetric with writing an integer to fix sign bits for negative
			// values...
			i = (i << 3) >> 3;
			value = new Integer(i);

			if (isDebug)
				trace.write(value);
			break;

		case kDoubleType:
			value = new Double(in.readDouble());

			if (isDebug)
				trace.write(value);
			break;

		case kUndefinedType:
			if (isDebug)
				trace.writeUndefined();
			break;

		case kNullType:

			if (isDebug)
				trace.writeNull();
			break;

		case kXMLType:
		case kAvmPlusXmlType:
			value = readXml();
			break;

		case kDateType:
			value = readDate();

			if (isDebug)
				trace.write(value.toString());
			break;

		case kByteArrayType:
			value = readByteArray();
			break;
		default:
			// Unknown object type tag {type}
			RuntimeException ex = new RuntimeException(
					"Unknown object type tag {" + type + "}");
			throw ex;
		}

		return value;
	}

	protected String readString() throws IOException {
		int ref = readUInt29();

		if ((ref & 1) == 0) {
			// This is a reference
			return getStringReference(ref >> 1);
		} else {
			// Read the string in
			int len = (ref >> 1);

			// writeString() special cases the empty string
			// to avoid creating a reference.
			if (0 == len) {
				return EMPTY_STRING;
			}

			String str = readUTF(len);

			// Remember String
			stringTable.add(str);

			return str;
		}
	}

	/**
	 * Deserialize the bits of a date-time value w/o a prefixing type byte.
	 */
	protected Date readDate() throws IOException {
		int ref = readUInt29();

		if ((ref & 1) == 0) {
			// This is a reference
			return (Date) getObjectReference(ref >> 1);
		} else {
			long time = (long) in.readDouble();

			Date d = new Date(time);

			// Remember Date
			objectTable.add(d);

			if (isDebug)
				trace.write(d);

			return d;
		}
	}

	protected Object readArray() throws ClassNotFoundException, IOException {
		int ref = readUInt29();

		if ((ref & 1) == 0) {
			// This is a reference
			return getObjectReference(ref >> 1);
		} else {
			int len = (ref >> 1);
			Object array = null;

			// First, look for any string based keys. If any
			// non-ordinal indices were used, or if the Array is
			// sparse, we represent the structure as a Map.
			Map map = null;
			for (;;) {
				String name = readString();
				if (name == null || name.length() == 0)
					break;

				if (map == null) {
					map = new HashMap();
					array = map;

					// Remember Object
					objectTable.add(array);

					if (isDebug)
						trace.startECMAArray(objectTable.size() - 1);
				}

				Object value = readObject();
				map.put(name, value);
			}

			// If we didn't find any string based keys, we have a
			// dense Array, so we represent the structure as a List.
			if (map == null) {
				List list = new ArrayList(len);
				array = list;

				// Remember List
				objectTable.add(array);

				if (isDebug)
					trace.startAMFArray(objectTable.size() - 1);

				for (int i = 0; i < len; i++) {
					if (isDebug)
						trace.arrayElement(i);

					Object item = readObject();
					list.add(i, item);
				}
			} else {
				for (int i = 0; i < len; i++) {
					if (isDebug)
						trace.arrayElement(i);

					Object item = readObject();
					map.put(Integer.toString(i), item);
				}
			}

			if (isDebug)
				trace.endAMFArray();

			return array;
		}
	}

	protected Object readScriptObject() throws ClassNotFoundException,
			IOException {
		int ref = readUInt29();

		if ((ref & 1) == 0) {
			return getObjectReference(ref >> 1);
		} else {
			TraitsInfo ti = readTraits(ref);
			String className = ti.getClassName();
			boolean externalizable = ti.isExternalizable();
			Object object;

			// Check for any registered class aliases
			String aliasedClass = classAliasRegistry.get(className);
			if (aliasedClass != null)
				className = aliasedClass;

			if (className == null || className.length() == 0) {
				object = new ASObject();
			} else if (className.indexOf(">") == 0) {
				// Handle [RemoteClass] (no server alias)
				object = new ASObject();
				((ASObject) object).setType(className);
			} else if (className.startsWith("flex.")) {
				String[] classParts = className.split("\\.");
				String unqualifiedClassName = classParts[(classParts.length - 1)];
				if (classesFlex.containsKey(unqualifiedClassName)) {
					object = new FlexClass();
				} else if (unqualifiedClassName.equals("ArrayCollection")){
					object= new FlexClassArrayCollection();
				} else {
					object = new ASObject();
					((ASObject) object).setType(className);
				}
			}

			else {
				// Just return type info with an ASObject...
				object = new ASObject();
				((ASObject) object).setType(className);
			}

			// Remember our instance in the object table
			int objectId = objectTable.size();
			objectTable.add(object);

			if (externalizable) {
				readExternalizable(className, object);
			} else {
				if (isDebug) {
					trace.startAMFObject(className, objectTable.size() - 1);
				}

				int len = ti.getProperties().size();

				for (int i = 0; i < len; i++) {
					String propName = (String) ti.getProperty(i);

					if (isDebug)
						trace.namedElement(propName);

					Object value = readObject();
					((ASObject) object).put(propName, value);
				}

				if (ti.isDynamic()) {
					for (;;) {
						String name = readString();
						if (name == null || name.length() == 0)
							break;

						if (isDebug)
							trace.namedElement(name);

						Object value = readObject();
						((ASObject) object).put(name, value);
					}
				}
			}

			if (isDebug)
				trace.endAMFObject();

			return object;
		}
	}

	protected void readExternalizable(String className, Object object) {
		// if (object instanceof Externalizable)
		// {
		if (isDebug) {
			trace.startExternalizableObject(className, objectTable.size() - 1);
		}

		try {
			((Externalizable) object).readExternal(this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// }
		// else
		// {
		// //Class '{className}' must implement java.io.Externalizable to
		// receive client IExternalizable instances.
		// RuntimeException ex = new RuntimeException("10305: Class " +
		// className +
		// " must implement java.io.Externalizable to receive client IExternalizable instances.");
		// throw ex;
		// }
	}

	protected byte[] readByteArray() throws IOException {
		int ref = readUInt29();

		if ((ref & 1) == 0) {
			return (byte[]) getObjectReference(ref >> 1);
		} else {
			int len = (ref >> 1);

			byte[] ba = new byte[len];

			// Remember byte array object
			objectTable.add(ba);

			in.readFully(ba, 0, len);

			if (isDebug)
				trace.startByteArray(objectTable.size() - 1, len);

			return ba;
		}
	}

	protected TraitsInfo readTraits(int ref) throws IOException {
		if ((ref & 3) == 1) {
			// This is a reference
			return getTraitReference(ref >> 2);
		} else {
			boolean externalizable = ((ref & 4) == 4);
			boolean dynamic = ((ref & 8) == 8);
			int count = (ref >> 4); /* uint29 */
			String className = readString();

			TraitsInfo ti = new TraitsInfo(className, dynamic, externalizable,
					count);

			// Remember Trait Info
			traitsTable.add(ti);

			for (int i = 0; i < count; i++) {
				String propName = readString();
				ti.addProperty(propName);
			}

			return ti;
		}
	}

	protected String readUTF(int utflen) throws IOException {
		char[] charr = getTempCharArray(utflen);
		byte[] bytearr = getTempByteArray(utflen);
		int c, char2, char3;
		int count = 0;
		int chCount = 0;

		in.readFully(bytearr, 0, utflen);

		while (count < utflen) {
			c = (int) bytearr[count] & 0xff;
			switch (c >> 4) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				/* 0xxxxxxx */
				count++;
				charr[chCount] = (char) c;
				break;
			case 12:
			case 13:
				/* 110x xxxx 10xx xxxx */
				count += 2;
				if (count > utflen)
					throw new UTFDataFormatException();
				char2 = (int) bytearr[count - 1];
				if ((char2 & 0xC0) != 0x80)
					throw new UTFDataFormatException();
				charr[chCount] = (char) (((c & 0x1F) << 6) | (char2 & 0x3F));
				break;
			case 14:
				/* 1110 xxxx 10xx xxxx 10xx xxxx */
				count += 3;
				if (count > utflen)
					throw new UTFDataFormatException();
				char2 = (int) bytearr[count - 2];
				char3 = (int) bytearr[count - 1];
				if (((char2 & 0xC0) != 0x80) || ((char3 & 0xC0) != 0x80))
					throw new UTFDataFormatException();
				charr[chCount] = (char) (((c & 0x0F) << 12)
						| ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0));
				break;
			default:
				/* 10xx xxxx, 1111 xxxx */
				throw new UTFDataFormatException();
			}
			chCount++;
		}
		// The number of chars produced may be less than utflen
		return new String(charr, 0, chCount);
	}

	/**
	 * AMF 3 represents smaller integers with fewer bytes using the most
	 * significant bit of each byte. The worst case uses 32-bits to represent a
	 * 29-bit number, which is what we would have done with no compression.
	 * 
	 * <pre>
	 * 0x00000000 - 0x0000007F : 0xxxxxxx
	 * 0x00000080 - 0x00003FFF : 1xxxxxxx 0xxxxxxx
	 * 0x00004000 - 0x001FFFFF : 1xxxxxxx 1xxxxxxx 0xxxxxxx
	 * 0x00200000 - 0x3FFFFFFF : 1xxxxxxx 1xxxxxxx 1xxxxxxx xxxxxxxx
	 * 0x40000000 - 0xFFFFFFFF : throw range exception
	 * </pre>
	 * 
	 */
	protected int readUInt29() throws IOException {
		int value;

		// Each byte must be treated as unsigned
		int b = in.readByte() & 0xFF;

		if (b < 128) {
			return b;
		}

		value = (b & 0x7F) << 7;
		b = in.readByte() & 0xFF;

		if (b < 128) {
			return (value | b);
		}

		value = (value | (b & 0x7F)) << 7;
		b = in.readByte() & 0xFF;

		if (b < 128) {
			return (value | b);
		}

		value = (value | (b & 0x7F)) << 8;
		b = in.readByte() & 0xFF;

		return (value | b);
	}

	protected Object readXml() throws IOException {
		String xml = null;

		int ref = readUInt29();

		if ((ref & 1) == 0) {
			// This is a reference
			xml = (String) getObjectReference(ref >> 1);
		} else {
			// Read the string in
			int len = (ref >> 1);

			// writeString() special case the empty string
			// for speed. Do add a reference
			if (0 == len)
				xml = EMPTY_STRING;
			else
				xml = readUTF(len);

			// Remember Object
			objectTable.add(xml);

			if (isDebug)
				trace.write(xml);
		}

		return stringToDocument(xml);
	}

	protected Object getObjectReference(int ref) {
		if (isDebug) {
			trace.writeRef(ref);
		}

		return objectTable.get(ref);
	}

	protected String getStringReference(int ref) {
		String str = (String) stringTable.get(ref);

		if (isDebug) {
			trace.writeStringRef(ref);
		}

		return str;
	}

	/**
	 * @exclude
	 */
	protected TraitsInfo getTraitReference(int ref) {
		if (isDebug) {
			trace.writeTraitsInfoRef(ref);
		}

		return (TraitsInfo) traitsTable.get(ref);
	}

}
