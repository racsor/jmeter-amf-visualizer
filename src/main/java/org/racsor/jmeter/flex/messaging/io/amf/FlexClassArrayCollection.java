package org.racsor.jmeter.flex.messaging.io.amf;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;

public class FlexClassArrayCollection extends ArrayList implements
		Externalizable {
	private static final long serialVersionUID = 8037277879661457358L;

	public FlexClassArrayCollection() {
		super();
	}

	public FlexClassArrayCollection(Collection c) {
		super(c);
	}

	public FlexClassArrayCollection(int initialCapacity) {
		super(initialCapacity);
	}

	public Object[] getSource() {
		return toArray();
	}

	public void setSource(Object[] s) {
		if (s != null) {
			if (size() > 0)
				clear();

			for (int i = 0; i < s.length; i++) {
				add(s[i]);
			}
		} else {
			clear();
		}
	}

	public void setSource(Collection s) {
		addAll(s);
	}

	public void readExternal(ObjectInput input) throws IOException,
			ClassNotFoundException {
		Object s = input.readObject();
		if (s instanceof Collection)
			s = ((Collection) s).toArray();
		Object[] source = (Object[]) s;
		setSource(source);
	}

	public void writeExternal(ObjectOutput output) throws IOException {
		output.writeObject(getSource());
	}

}
