/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2008 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

package org.racsor.flex.connect;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.racsor.jmeter.flex.messaging.util.UtilsFlexMessage;

import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.io.amf.client.exceptions.ServerStatusException;

/**
 * A simple test that creates a sample AMF request from an xml file and then
 * pipes the output back into the AMF MessageDeserializer.
 * <p/>
 * A metric for the current build is recorded as "AMF Deserialization Time" in
 * milliseconds.
 * </p>
 * 
 * @author Peter Farland
 */
public class CC_connectTest extends TestCase {

    public CC_connectTest(String name) {
	super(name);
    }

    protected void setUp() {
    }

    public static void main(String args[]) {
	junit.textui.TestRunner.run(suite());
    }

    public static Test suite() {
	return new TestSuite(CC_connectTest.class);
    }

    public void testMultiple() throws Exception {
	for (int i = 0; i < 10; i++) {
	    _testDoJobAndCollectMoneyFairview();
	}
    }

    public void _testDoJobAndCollectMoneyFairview() throws Exception {
	HttpURLConnection connection = null;

	File docSerializeMoney = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\pickupItemMoney.binary");
	File docSerializeCollection = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\pickupItemCollection.binary");
	File doc = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\doJob_car_Fairview.binary");
//	File doc = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\doJob_bagel_Hillcrest.binary");
	byte[] theFile = FileUtils.readFileToByteArray(doc);

	//Create connection
	String urlString = "http://ct-google.crimecitygame.com/ct-google/index.php/amf_gateway?f=104995124568475809013&r=43&t=none";
	String contentType = "application/x-amf";
	URL url = new URL(urlString);

	connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setRequestProperty("Connection", "keep-alive");
	connection.setRequestProperty("Content-Type", contentType);
	//	    connection.setRequestProperty("Content-Length", "" + xurro.length);

	//	    connection.setUseCaches(false);
	connection.setDoInput(true);
	connection.setDoOutput(true);

	//Send request
	DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	wr.write(theFile);
	wr.flush();
	wr.close();

	System.out.println("responsecode:" + connection.getResponseCode());

	//Get Response	
	InputStream is = connection.getInputStream();
//	FileUtils.writeByteArrayToFile(new File("\\responseFairview.binary"), IOUtils.toByteArray(is));

	UtilsFlexMessage utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(is);

	//Get pickupItem
	String content = utilsFlex.messageToXML();
	System.out.println(content);
	String pickupItem = "pickup_item:" + StringUtils.substringBetween(content, "<string>pickup_item:", "</string>");
	System.out.println(pickupItem);
	String oldPickupItem = "pickup_item:276698290864898721:917:dn34ppe0ireh";

	//Get template pickupItemMoney and update with new item get above
	theFile = FileUtils.readFileToByteArray(docSerializeMoney);
	ByteArrayInputStream bais = new ByteArrayInputStream(theFile);
	DataInputStream din = new DataInputStream(bais);
	utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(din);
	String update = utilsFlex.updateFlexMessage(oldPickupItem, pickupItem);
	byte[] newdataPickupItem = utilsFlex.serializeMessage(update);

	//new Connection to pickup Money
	connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setRequestProperty("Connection", "keep-alive");
	connection.setRequestProperty("Content-Type", contentType);
	//	    connection.setRequestProperty("Content-Length", "" + xurro.length);

	//	    connection.setUseCaches(false);
	connection.setDoInput(true);
	connection.setDoOutput(true);

	//Send request
	wr = new DataOutputStream(connection.getOutputStream());
	wr.write(newdataPickupItem);
	wr.flush();
	wr.close();

	System.out.println("responsecode:" + connection.getResponseCode());

	//Get template pickupItemCollection and update with new item get above
	int temp = StringUtils.indexOf(content, "<string>collection_loot_item_id</string>\n                  <null/>");
	System.out.println(temp);
	if (temp != -1) {
	    theFile = FileUtils.readFileToByteArray(docSerializeCollection);
	    bais = new ByteArrayInputStream(theFile);
	    din = new DataInputStream(bais);
	    utilsFlex = new UtilsFlexMessage();
	    utilsFlex.parseInputStream(din);
	    update = utilsFlex.updateFlexMessage(oldPickupItem, pickupItem);
	    newdataPickupItem = utilsFlex.serializeMessage(update);
	    //new Connection to pickup ItemCollection
	    connection = (HttpURLConnection) url.openConnection();
	    connection.setRequestMethod("POST");
	    connection.setRequestProperty("Connection", "keep-alive");
	    connection.setRequestProperty("Content-Type", contentType);
	    //	    connection.setRequestProperty("Content-Length", "" + xurro.length);

	    //	    connection.setUseCaches(false);
	    connection.setDoInput(true);
	    connection.setDoOutput(true);

	    //Send request
	    wr = new DataOutputStream(connection.getOutputStream());
	    wr.write(newdataPickupItem);
	    wr.flush();
	    wr.close();

	    System.out.println("responsecode:" + connection.getResponseCode());
	}

	//	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	//	String line;
	//	StringBuffer response = new StringBuffer();
	//	while ((line = rd.readLine()) != null) {
	//	    response.append(line);
	//	    response.append('\r');
	//	}
	//	rd.close();
	//	System.out.println("response:" + response.toString());

    }

    public void _testUrlConnectionCC() throws Exception {
	HttpURLConnection connection = null;

	//	File doc = new File(Thread.currentThread().getContextClassLoader().getResource("attackbossNattyBlunt.binary").getFile());
	//	File doc = new File(Thread.currentThread().getContextClassLoader().getResource("bosses_attack.binary").getFile());
	//	File docSerialize = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\pickupItemMoney.binary");
	File doc = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\doJob_car_Fairview.binary");
	byte[] theFile = FileUtils.readFileToByteArray(doc);

	//Create connection
	String urlString = "http://ct-google.crimecitygame.com/ct-google/index.php/amf_gateway?f=104995124568475809013&r=43&t=none";
	String content = "application/x-amf";
	URL url = new URL(urlString);

	connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setRequestProperty("Connection", "keep-alive");
	connection.setRequestProperty("Content-Type", content);
	//	    connection.setRequestProperty("Content-Length", "" + xurro.length);

	//	    connection.setUseCaches(false);
	connection.setDoInput(true);
	connection.setDoOutput(true);

	//Send request
	DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	wr.write(theFile);
	wr.flush();
	wr.close();

	System.out.println("responsecode:" + connection.getResponseCode());

	//Get Response	
	InputStream is = connection.getInputStream();
	//	FileUtils.writeByteArrayToFile(new File("\\responseFairview.binary"), IOUtils.toByteArray( is ));

	UtilsFlexMessage deserializer = new UtilsFlexMessage();
	deserializer.parseInputStream(is);

	//	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	//	String line;
	//	StringBuffer response = new StringBuffer();
	//	while ((line = rd.readLine()) != null) {
	//	    response.append(line);
	//	    response.append('\r');
	//	}
	//	rd.close();
	//	System.out.println("response:" + response.toString());

    }

    public void _testConnectionCC() {
	try {
	    AMFConnection amfConnection = new AMFConnection();

	    String url = "http://ct-google.crimecitygame.com/ct-google/index.php/amf_gateway?f=104995124568475809013&r=43&t=none";
	    String service = "BatchController.call";
	    try {
		amfConnection.connect(url);
	    } catch (ClientStatusException cse) {
		System.out.println(cse);
		return;
	    }
	    // Make a remoting call and retrieve the result.
	    try {
		Object result = amfConnection.call(service);
		System.out.println("results: " + result.toString());
	    } catch (ClientStatusException cse) {
		System.out.println(cse);
	    } catch (ServerStatusException sse) {
		System.out.println(sse);
	    }

	    // Close the connection.
	    amfConnection.close();

	} catch (Exception e) {
	    e.printStackTrace();
	    fail();
	}
    }

}
