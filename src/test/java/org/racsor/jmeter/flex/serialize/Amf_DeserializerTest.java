/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2008 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

package org.racsor.jmeter.flex.serialize;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.racsor.jmeter.flex.messaging.io.amf.ASObject;
import org.racsor.jmeter.flex.messaging.io.amf.ActionMessage;
import org.racsor.jmeter.flex.messaging.io.amf.AmfMessageDeserializer;
import org.racsor.jmeter.flex.messaging.io.amf.AmfTrace;
import org.racsor.jmeter.flex.messaging.io.amf.MessageBody;

/**
 * A simple test that creates a sample AMF request from an xml file and then
 * pipes the output back into the AMF MessageDeserializer.
 * <p/>
 * A metric for the current build is recorded as "AMF Deserialization Time" in
 * milliseconds.
 * </p>
 * 
 * @author Peter Farland
 */
public class Amf_DeserializerTest extends TestCase {

    public Amf_DeserializerTest(String name) {
	super(name);
    }

    protected void setUp() {
    }

    public static void main(String args[]) {
	junit.textui.TestRunner.run(suite());
    }

    public static Test suite() {
	return new TestSuite(Amf_DeserializerTest.class);
    }

    /**
     * Test creating an AMF message and then sending it through an AMFEndpoint
     */
    public void testDeserializeMessage() {
	AmfTrace trace = new AmfTrace();
	try {

	    /**
	     * GENERATE SAMPLE AMF REQUEST FROM DATA FILE
	     */

//	    File doc = new File(Thread.currentThread().getContextClassLoader().getResource("serialize/collectArcade_1h.binary").getFile());
//	    File doc = new File(Thread.currentThread().getContextClassLoader().getResource("serialize/depositBank10000.binary").getFile());
	    File doc = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\bosses_attack.binary");
	    byte[] theFile = FileUtils.readFileToByteArray(doc);
	    ByteArrayInputStream bais = new ByteArrayInputStream(theFile);
	    DataInputStream din = new DataInputStream(bais);

	    /**
	     * CREATE A DESERIALIZER FOR SAMPLE AMF REQUEST
	     */
	    ActionMessage message = new ActionMessage();
	    trace.startRequest("----------Deserializing AMF/HTTP request-----");

	    AmfMessageDeserializer deserializer = new AmfMessageDeserializer();
	    deserializer.initialize(din, trace);

	    /**
	     * RECORD TIME TO DESERIALIZE THE SAMPLE MESSAGE AS OUR TEST
	     * METRIC...
	     */
	    long start = System.currentTimeMillis();

	    deserializer.readMessage(message);
	    
	    long finish = System.currentTimeMillis();
	    trace.endMessage();
	    long duration = finish - start;

	    /**
	     * PRINT TRACE OUTPUT
	     */
	    System.out.print(trace.toString());

	    System.out.print("AMF Deserialization Time: " + duration + "ms");

	} catch (Exception e) {
	    System.out.println(trace.toString());
	    e.printStackTrace();
	    fail();
	}
    }

    public void _testDeserializeMultipleMessage() {
	try {

	    /**
	     * GENERATE SAMPLE AMF REQUEST FROM DATA FILE
	     */
	    File doc = new File(Thread.currentThread().getContextClassLoader().getResource("serialize/POST5380304249064995992.binary").getFile());
	    System.out.println(doc.getParent());
	    File dir = new File(doc.getParent());
//	    File dir = doc;
	    System.out.println(dir.getPath());
	    String[] files = dir.list(new SuffixFileFilter(".binary"));
	    for (int i = 0; i < files.length; i++) {
		AmfTrace trace = new AmfTrace();
		System.out.println("===================================================");
		System.out.println(files[i]);
		System.out.println(dir.getPath()+File.separatorChar+files[i]);

		byte[] theFile = FileUtils.readFileToByteArray(new File(dir.getPath()+File.separatorChar+files[i]));
		ByteArrayInputStream bais = new ByteArrayInputStream(theFile);
		DataInputStream din = new DataInputStream(bais);

		/**
		 * CREATE A DESERIALIZER FOR SAMPLE AMF REQUEST
		 */
		ActionMessage message = new ActionMessage();
		trace.startRequest("----------Deserializing AMF/HTTP request-----");

		AmfMessageDeserializer deserializer = new AmfMessageDeserializer();
		deserializer.initialize(din, trace);

		/**
		 * RECORD TIME TO DESERIALIZE THE SAMPLE MESSAGE AS OUR TEST
		 * METRIC...
		 */
		long start = System.currentTimeMillis();

		deserializer.readMessage(message);

		long finish = System.currentTimeMillis();
		trace.endMessage();
		long duration = finish - start;

		/**
		 * PRINT TRACE OUTPUT
		 */
		System.out.print(trace.toString());

		System.out.print("AMF Deserialization Time: " + duration + "ms");
		System.out.println("===================================================");
	    }

	} catch (Exception e) {
//	    System.out.println(trace.toString());
	    e.printStackTrace();
	    fail();
	}
    }
}
