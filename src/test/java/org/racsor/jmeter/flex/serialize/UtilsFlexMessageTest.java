package org.racsor.jmeter.flex.serialize;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.racsor.jmeter.flex.messaging.io.amf.ActionMessage;
import org.racsor.jmeter.flex.messaging.io.amf.AmfMessageDeserializer;
import org.racsor.jmeter.flex.messaging.io.amf.AmfTrace;
import org.racsor.jmeter.flex.messaging.util.UtilsFlexMessage;

public class UtilsFlexMessageTest extends TestCase {
    protected void setUp() {
    }

    public static void main(String args[]) {
	junit.textui.TestRunner.run(suite());
    }

    public static Test suite() {
	return new TestSuite(UtilsFlexMessageTest.class);
    }

    public void _testSerializeDeserializeMessage() throws Exception {
//	File doc = new File(Thread.currentThread().getContextClassLoader()
	//			.getResource("_POST2655.binary.Crack").getFile());
	//	.getResource("serializer.binary").getFile());
	//	.getResource("POST62083.binary").getFile());
//		.getResource("serialize/depositBank10000.binary").getFile());
	//		.getResource("serialize/collectHouse1_24h.binary").getFile());
	File doc = new File("\\responseFairview.binary");
	byte[] theFile = FileUtils.readFileToByteArray(doc);
	ByteArrayInputStream bais = new ByteArrayInputStream(theFile);
	DataInputStream din = new DataInputStream(bais);
	UtilsFlexMessage utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(din);
	String content=utilsFlex.messageToXML();
	System.out.println(content);
	String pickupItem="pickup_item:"+StringUtils.substringBetween(content, "<string>pickup_item:", "</string>");
	System.out.println(pickupItem);
	String temp=""+StringUtils.indexOf(content, "<string>collection_loot_item_id</string>\n                  <null/>");
	System.out.println(temp);
	
	String oldPickupItem="pickup_item:276698290864898721:917:rflinzqcft68";
	
	doc = new File("C:\\TIC_LOCAL\\EclipseProjects\\OpenSourceRacsor\\cc_request\\jmeter\\pickupItemMoney.binary");
	theFile = FileUtils.readFileToByteArray(doc);
	bais = new ByteArrayInputStream(theFile);
	din = new DataInputStream(bais);
	utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(din);
	String update=utilsFlex.updateFlexMessage(oldPickupItem, pickupItem);
	utilsFlex.serializeMessage("\\serialize.binary",update);
	
	DataInputStream dini = new DataInputStream(new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("\\serialize.binary"))));
	utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(dini);
	
	
	
	
//	String content=utilsFlex.updateFlexMessage("arhacea4c4", "6xgekrsjv6");
//	utilsFlex.serializeMessage("\\serialize.binary",content);
//
//	DataInputStream dini = new DataInputStream(new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("\\serialize.binary"))));
//	utilsFlex = new UtilsFlexMessage();
//	utilsFlex.parseInputStream(dini);
    }
    public void _testSerializeDeserializeMessage1() throws Exception {
	File doc = new File(Thread.currentThread().getContextClassLoader()
		//			.getResource("_POST2655.binary.Crack").getFile());
		//	.getResource("serializer.binary").getFile());
		//	.getResource("POST62083.binary").getFile());
		.getResource("serialize/depositBank10000.binary").getFile());
	//		.getResource("serialize/collectHouse1_24h.binary").getFile());
	// File doc = new File(Thread.currentThread().getContextClassLoader().getResource("POST43415.binary").getFile());
	byte[] theFile = FileUtils.readFileToByteArray(doc);
	ByteArrayInputStream bais = new ByteArrayInputStream(theFile);
	DataInputStream din = new DataInputStream(bais);
	UtilsFlexMessage utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(din);
	String content=utilsFlex.updateFlexMessage("arhacea4c4", "6xgekrsjv6");
	utilsFlex.serializeMessage("\\serialize.binary",content);
	
	DataInputStream dini = new DataInputStream(new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("\\serialize.binary"))));
	utilsFlex = new UtilsFlexMessage();
	utilsFlex.parseInputStream(dini);
    }

    public void testSerializeDeserializeMessageMultiple() throws Exception {
	File doc = new File("c:/jmeter/01-load_all_static_store.binary");
	System.out.println(doc.getParent());
	File dir = new File(doc.getParent());
	//	    File dir = doc;
	System.out.println(dir.getPath());
	String[] files = dir.list(new SuffixFileFilter(".binary"));
	for (int i = 0; i < files.length; i++) {
	    System.out.println("===================================================");
	    System.out.println(files[i]);
	    String filename=dir.getPath() + File.separatorChar + files[i];
	    System.out.println(filename);

	    byte[] theFile = FileUtils.readFileToByteArray(new File(filename));
	    ByteArrayInputStream bais = new ByteArrayInputStream(theFile);
	    DataInputStream din = new DataInputStream(bais);
	    UtilsFlexMessage utilsFlex = new UtilsFlexMessage();
	    utilsFlex.parseInputStream(din);
	    String content=utilsFlex.updateFlexMessage("2288iduzo6", "65cbmmj34v");
	    utilsFlex.serializeMessage(filename,content);
	}

    }

}
